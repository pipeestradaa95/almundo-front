# Prueba Tecnica FRONT END

En este repo se encuentra el front end desarrollado con angular 5 y typescript.

## Ejecutar Proyecto

En primera instancia se deben descargar las dependencias, esto se hace por medio de **npm install**.
Para desplegar el proyecto se debe lanzar el comando **npm run start** y ejecutar el express server desarrollado en el punto anterior.
