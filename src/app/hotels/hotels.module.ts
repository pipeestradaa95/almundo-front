import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { HotelComponent } from './components/hotels.component';
import { HotelCardsComponent } from './components/hotel-cards/hotel-cards.component';
import { HotelCardComponent } from './components/hotel-cards/hotel-card/hotel-card.component';
import { HotelsService } from './services/hotels.service';



@NgModule({
  declarations: [
    HotelComponent,
    HotelCardsComponent,
    HotelCardComponent
  ],
  imports: [BrowserModule,FormsModule],
  providers: [HotelsService],
  bootstrap: [
    HotelComponent,
    HotelCardsComponent,
    HotelCardComponent
  ],
  exports:[
    HotelComponent
  ]
})
export class HotelModule { }
