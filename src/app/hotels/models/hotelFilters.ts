export interface HotelFilter {
 name: string,
 stars: Array<number>
}
