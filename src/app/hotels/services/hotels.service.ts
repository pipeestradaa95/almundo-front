// core libraries
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

// rx for http services
import { Observable } from 'rxjs';
import 'rxjs/Rx';


// environment configurations
import { environment } from '../../../environments/environment';

// mappings
import { HotelMapping } from '../mappings/hotel.mapping';

// base service
import { ApiService } from '../../commons/services/api.service';

// entities
import { Hotel } from '../models/hotel';

@Injectable()
export class HotelsService extends ApiService {

  constructor(private http : Http) {
    super();
  }

  getHotels(): Observable<Array<Hotel>> {
    return this.http
          .get(`${environment.apiUrl}/hotels`, { headers: this.getHeaders() })
          .map(HotelMapping.mapHotel)
          .catch(this.handleError);
  }

  getHotelsWithFilter(name: string, stars: Array<number>): Observable<Array<Hotel>>{
    return this.http
           .put(`${environment.apiUrl}/hotels`, {name: name, stars: stars },{ headers: this.getHeaders() })
           .map(HotelMapping.mapHotel)
           .catch(this.handleError);
  }

  private handleError(error: any) {
    let errorMsg = error.message || 'Ocurrió un error, por favor intente nuevamente más tarde.';
    return Observable.throw(errorMsg);
  }
}
