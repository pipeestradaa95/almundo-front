import { Component, Input } from '@angular/core';
import { Hotel } from '../../../models/hotel'

@Component({
  selector: 'hotel-card',
  templateUrl: './hotel-card.component.html',
  styleUrls: ['./hotel-card.component.css']
})
export class HotelCardComponent {
  hotel: Hotel;
  hotelName: string = "";
  hotelImage: string = "assets/images/hotels/";
  stars: number = 0;
  price: number = 0;
  amenities: Array<string> = [];

  starsArray = Array;

  @Input() set hotelCard(_hotel: Hotel){
    if(_hotel !== undefined){
      this.hotel = _hotel;

      this.hotelName = _hotel.name;
      this.hotelImage += _hotel.image;
      this.stars = _hotel.stars;
      this.price = _hotel.price;
      this.amenities = _hotel.amenities.map(element => {
        return "assets/icons/amenities/"+element+".svg";
      });
    }
  }

  constructor(){
  }
}
