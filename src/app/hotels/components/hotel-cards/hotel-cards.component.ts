import { Component, Input } from '@angular/core';
import { Hotel } from '../../models/hotel'

@Component({
  selector: 'hotel-cards',
  templateUrl: './hotel-cards.component.html',
  styleUrls: ['./hotel-cards.component.css']
})
export class HotelCardsComponent {
  listHotel: Array<Hotel>;

  @Input() set listHotelFilter(list: Array<Hotel>){
    if(list !== undefined){
      this.listHotel = list;
    }
  }

  constructor(){
  }
}
