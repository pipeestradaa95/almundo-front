import { Component } from '@angular/core';
import { Hotel } from '../models/hotel';
import { HotelsService } from "../services/hotels.service";

@Component({
  selector: 'hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.css']
})
export class HotelComponent {

  listHotel: Array<Hotel>;
  hotelName: string;
  arrayStars: Array<number>;
  oneStar: boolean = false;
  twoStars: boolean  = false;
  threeStars: boolean  = false ;
  fourStars: boolean  = false ;
  fiveStars: boolean  = false ;
  allStars: boolean  = true;

  arrayFor = Array;

  constructor(private hotelService: HotelsService){
    this.hotelService.getHotels().subscribe(list => {
      this.listHotel = list;
    }, error => {
      console.log("HUBO UN ERROR", error);
    });
  }

  filter(){
    this.setStars();
    if(this.hotelName === ""){
      this.hotelName = undefined;
    }
    this.hotelService.getHotelsWithFilter(this.hotelName,this.arrayStars).subscribe(list => {
      this.listHotel = list;
    }, error => {
      console.log("HUBO UN ERROR", error);
    });
  }

  setAllStars(){
    if(this.allStars === true){
      this.oneStar = false;
      this.twoStars = false;
      this.threeStars = false;
      this.fourStars = false;
      this.fiveStars = false;
    }
    this.filter();
  }

  setStars(){
    this.arrayStars = [];
    if(this.allStars === true){
      this.arrayStars = [1, 2, 3, 4, 5];
    }else{
      if(this.oneStar === true){
        this.arrayStars.push(1);
      }
      if(this.twoStars === true){
        this.arrayStars.push(2);
      }
      if(this.threeStars === true){
        this.arrayStars.push(3);
      }
      if(this.fourStars === true){
        this.arrayStars.push(4);
      }
      if(this.fiveStars === true){
        this.arrayStars.push(5);
      }
    }
  }

}
