import { Response } from '@angular/http';
import { Hotel } from '../models/hotel';

export class HotelMapping {

  static  mapHotel(response: Response): Hotel {
    return response.json().map(HotelMapping.toHotel);
  }

  static responseToHotel(response: Response): Hotel{
    return HotelMapping.toHotel(response.json());
  }

  static toHotel(hotel: any): Hotel {
    return <Hotel> {
      id: hotel.id,
      name: hotel.name,
      stars: hotel.stars,
      price: hotel.price,
      image: hotel.image,
      amenities: hotel.amenities
    }
  }
}
