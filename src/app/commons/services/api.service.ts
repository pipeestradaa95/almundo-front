import { Headers } from '@angular/http';

export class ApiService {

  protected getHeaders(): Headers {
    let headers = new Headers();

    headers.append('Accept', 'application/json');

    return headers;
  }
}
